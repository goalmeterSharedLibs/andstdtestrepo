package com.weightmeter.facebookauthentication;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class FacebookHelper {
    private FacebookSignInListener mListener;
    private CallbackManager mCallBackManager;
    private FirebaseAuth mAuth;

    private ReadPermission[] permissions = {ReadPermission.EMAIL,
            ReadPermission.PUBLIC_PROFILE,
            ReadPermission.USER_FRIENDS};

    private LoginButton loginButton;

    private boolean registerWithButton = false;

    public FacebookHelper(FacebookSignInListener facebookSignInListener) {
        mListener = facebookSignInListener;
        mCallBackManager = CallbackManager.Factory.create();
        FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                mListener.onCancel();
            }

            @Override
            public void onError(FacebookException e) {
                mListener.onFail(e.getMessage());
            }
        };
        LoginManager.getInstance().registerCallback(mCallBackManager, mCallBack);

        mAuth = FirebaseAuth.getInstance();
    }

    public FacebookHelper(LoginButton loginButton, FacebookSignInListener facebookSignInListener) {
        mListener = facebookSignInListener;
        this.loginButton = loginButton;
        mCallBackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(mCallBackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                mListener.onCancel();
            }

            @Override
            public void onError(FacebookException e) {
                mListener.onFail(e.getMessage());
            }
        });

        mAuth = FirebaseAuth.getInstance();
        registerWithButton = true;
    }

    public void setPermissions(ReadPermission[] permissions) {
        this.permissions = permissions;
        if (loginButton != null && registerWithButton) {
            loginButton.setPublishPermissions(getPermissionAsString());
        }
    }

    public CallbackManager getCallbackManager() {
        return mCallBackManager;
    }

    public void performSignIn(Activity activity) {
        LoginManager.getInstance()
                .logInWithReadPermissions(activity, getPermissionAsString());
    }

    public void performSignIn(Fragment fragment) {
        LoginManager.getInstance()
                .logInWithReadPermissions(fragment, getPermissionAsString());
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallBackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void performSignOut() {
        LoginManager.getInstance().logOut();
    }

    private List<String> getPermissionAsString() {
        List<String> permissions = new ArrayList<>();
        for (ReadPermission permission : this.permissions) {
            permissions.add(permission.toString());
        }

        return permissions;
    }

    private void handleFacebookAccessToken(AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {

                        if (!task.isSuccessful()) {
                            mListener.onFirebaseFail(task.getException().getMessage());
                        } else {
                            mListener.onSuccess(mAuth.getCurrentUser());
                        }
                    }
                });
    }
}