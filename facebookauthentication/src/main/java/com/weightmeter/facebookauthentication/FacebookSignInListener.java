package com.weightmeter.facebookauthentication;

import com.google.firebase.auth.FirebaseUser;

public interface FacebookSignInListener {
    void onFail(String errorMessage);

    void onFirebaseFail(String errorMessage);

    void onSuccess(FirebaseUser user);

    void onCancel();
}