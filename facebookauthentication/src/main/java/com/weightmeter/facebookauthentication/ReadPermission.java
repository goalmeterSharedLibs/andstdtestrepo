package com.weightmeter.facebookauthentication;

public enum ReadPermission {

    PUBLIC_PROFILE("public_profile"),
    USER_FRIENDS("user_friends"),
    EMAIL("email");

    private final String permission;

    ReadPermission(final String permission) {
        this.permission = permission;
    }
    @Override
    public String toString() {
        return permission;
    }
}
